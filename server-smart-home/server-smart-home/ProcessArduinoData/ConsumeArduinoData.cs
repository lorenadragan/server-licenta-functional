﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using server_smart_home.HubConfig;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace server_smart_home.ProcessArduinoData
{
    public class ConsumeArduinoData : IHostedService, IDisposable
    {
        private readonly ILogger<ConsumeArduinoData> _logger;
        private HttpClient _httpClient;
        private Timer timer;
        private readonly string baseURI = "http://192.168.4.1/";
        private IHubContext<SensorsHub> _sensorsHub;
        public ConsumeArduinoData(ILogger<ConsumeArduinoData> logger, IHubContext<SensorsHub> sensorsHub)
        {
            this._sensorsHub = sensorsHub;
            this._logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();

            this._httpClient = new HttpClient();
            this._httpClient.Timeout = TimeSpan.FromMinutes(30);

            this._httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
            this._httpClient.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            this._httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
            this._httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
            this._httpClient.DefaultRequestHeaders.Add("Accept-Language", "ro-RO,ro;q=0.9,en-US;q=0.8,en;q=0.7");
            this._httpClient.DefaultRequestHeaders.Add("Host", "192.168.4.1");

            var jsonAsString = "";
            timer = new Timer(o =>
            {
                try
                {
                    using (HttpResponseMessage response = this._httpClient.GetAsync(this.baseURI).Result)
                    {

                        using (HttpContent content = response.Content)
                        {
                            jsonAsString = content.ReadAsStringAsync().Result;
                            Debug.WriteLine(jsonAsString);
                            _sensorsHub.Clients.All.SendAsync("transfersensorsdata", jsonAsString);
                            jsonAsString = "";
                            this._logger.LogInformation(jsonAsString);

                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            },
            null,
            TimeSpan.Zero,
            TimeSpan.FromSeconds(15));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            timer?.Dispose();
        }

    }
}
