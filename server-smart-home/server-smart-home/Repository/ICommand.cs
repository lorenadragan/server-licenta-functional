﻿using server_smart_home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Repository
{
    public interface ICommand<TModel>
        where TModel : class, IPersistableModel
    {
        TModel Insert(TModel newObject);

        TModel Update(TModel obj);

        void Delete(object id);

        void Delete(TModel entityToDelete);

        void Dispose(bool disposing);

    }
}
