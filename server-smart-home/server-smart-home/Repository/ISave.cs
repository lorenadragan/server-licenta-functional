﻿using server_smart_home.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Repository
{
    public interface ISave<TModel>
         where TModel : class, IPersistableModel
    {
        void SaveChangesMethod();

        void OnSaveValidateErrors(DbEntityValidationException dbEx);
    }
}
