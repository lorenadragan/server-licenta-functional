﻿using server_smart_home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Repository
{
    public class InhabitantsService
    {
        private SmartHouseContext _context;
        private Repository<Inhabitant> _repository;

        public InhabitantsService(SmartHouseContext context)
        {
            this._context = context;
            this._repository = new Repository<Inhabitant>(this._context);
        }

        public void AddNewInhabitant(Inhabitant newInhabitant)
        {
            this._repository.Insert(newInhabitant);
            this._repository.SaveChangesMethod();
        }

        public void DeleteInhabitantByUserId(string userId)
        {
            var inhabitantByUserid = this._repository.GetAll().ToList().Where(inhab => inhab.User_Id == userId).First();
            this._repository.Delete(inhabitantByUserid);
            this._repository.SaveChangesMethod();
        }
    }
}
