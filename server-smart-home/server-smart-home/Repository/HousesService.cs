﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using server_smart_home.Models;
using server_smart_home.Models.DTOs;

namespace server_smart_home.Repository
{
    public class HousesService
    {
        private SmartHouseContext _context;
        private Repository<House> _housesRepository;
        private Repository<Inhabitant> _inhabitantsRepository;
        private UserManager<ApplicationUser> _userManager;
        public HousesService(UserManager<ApplicationUser> userManager, SmartHouseContext context)
        {
            this._context = context;
            this._housesRepository = new Repository<House>(this._context);
            this._inhabitantsRepository = new Repository<Inhabitant>(this._context);
            this._userManager = userManager;
        }

        public House GetHouseWithGivenAccessCode(string accessCode)
        {
            var houses = this._housesRepository.GetAll();
            foreach (var house in houses)
            {
                if (string.Equals(house.AccessCode, accessCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    return house;
                }
            }
            return null;
        }

        public async Task<HouseDTO> GetHouseDTODetailsForHomePage(string accessCode)
        {
            var houses = this._housesRepository.GetAll().ToList();
            foreach (var house in houses)
            {
                if (string.Equals(house.AccessCode, accessCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    var listOfInhabitantsDTO = new List<InhabitantDTO>();
                    //iau Inhabitants, dupa inhabitantsDTO, si dupa fac lista si adaug la casa DTO
                    var inhabitantsFromCurrentHouse = this._inhabitantsRepository.GetAll().ToList().Where(s => s.House_Id == house.Id);
                    foreach (var inhabitant in inhabitantsFromCurrentHouse)
                    {
                        var inhabitantDTO = new InhabitantDTO
                        {
                            User_Id = inhabitant.User_Id,
                            House_Id = house.Id,
                            Inhabitant_Name = (await this._userManager.FindByIdAsync(inhabitant.User_Id)).FullName
                        };
                        listOfInhabitantsDTO.Add(inhabitantDTO);
                    }

                    var houseDTO = new HouseDTO()
                    {
                        Address = house.Address,
                        AccessCode = house.AccessCode,
                        Surface = house.Surface,
                        RoomsNumber = house.RoomsNumber,
                        InhabitantDTOs = listOfInhabitantsDTO
                    };

                    return houseDTO;
                }
            }
            return null;
        }

        public void AddHouse(HouseModel model)
        {
            var house = new House()
            {
                AccessCode = model.AccessCode,
                Address = model.Address,
                Surface = model.Surface,
                RoomsNumber = model.RoomsNumber
            };

            this._housesRepository.Insert(house);
            this._housesRepository.SaveChangesMethod();
        }

        public List<HouseModel> GetHousesForAdminPanel()
        {
            var houses = this._housesRepository.GetAll();
            var housesModels = new List<HouseModel>();
            foreach (var house in houses)
            {
                var model = new HouseModel()
                {
                    AccessCode = house.AccessCode,
                    Surface = house.Surface,
                    Address = house.Address,
                    RoomsNumber = house.RoomsNumber
                };
                housesModels.Add(model);
            }

            return housesModels;
        }

        public int GetIdOfHouseWithGivenCode(string accessCode)
        {
            var houses = this._housesRepository.GetAll();
            foreach (var house in houses)
            {
                if (string.Equals(house.AccessCode, accessCode, StringComparison.InvariantCultureIgnoreCase))
                {
                    return house.Id;
                }
            }

            return -1;
        }

    }
}
