﻿using server_smart_home.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Repository
{
    public interface IRead<TModel>
        where TModel : class, IPersistableModel
    {
        TModel GetById(object Id);
        IQueryable<TModel> GetAll();
    }
}
