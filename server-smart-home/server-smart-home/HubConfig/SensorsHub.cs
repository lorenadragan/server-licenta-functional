﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.HubConfig
{
    public class SensorsHub : Hub
    {
        public Task Send(string stringJsonSensors)
        {
            return Clients.All.SendAsync("transfersensorsdata", stringJsonSensors);
        }
    }
}
