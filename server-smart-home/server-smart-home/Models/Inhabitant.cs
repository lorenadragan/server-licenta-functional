﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Models
{
    public class Inhabitant : IPersistableModel
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string User_Id { get; set; }

        [Column(TypeName = "int")]
        public int House_Id { get; set; }
    }
}
