﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Models
{
    public class House : IPersistableModel
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Address { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string AccessCode { get; set; }

        [Column(TypeName = "float")]
        public double Surface { get; set; }

        [Column(TypeName = "int")]
        public int RoomsNumber { get; set; }
    }
}
