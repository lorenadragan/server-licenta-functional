﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Models.DTOs
{
    public class HomePageProfileInformation
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string AccessCode { get; set; }
        public HouseDTO HouseDetails { get; set; }
    }
}
