﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Models.DTOs
{
    public class HouseDTO
    {
        public HouseDTO() { }

        public string Address { get; set; }
        public string AccessCode { get; set; }
        public double Surface { get; set; }
        public int RoomsNumber { get; set; }

        public List<InhabitantDTO> InhabitantDTOs { get; set; }
    }
}
