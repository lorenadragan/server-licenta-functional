﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Models.DTOs
{
    public class InhabitantDTO
    {
        public InhabitantDTO() { }

        public string User_Id { get; set; }
        public int House_Id { get; set; }

        public string Inhabitant_Name { get; set; }
    }
}
