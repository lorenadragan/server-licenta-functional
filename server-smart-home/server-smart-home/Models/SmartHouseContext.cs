﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Models
{
    public class SmartHouseContext : DbContext
    {
        public SmartHouseContext(DbContextOptions<SmartHouseContext> options) : base(options)
        {

        }

        //the IP the module generated
        public readonly string baseURIDefinition = "http://192.168.4.1/";
        public DbSet<House> Houses { get; set; }
        public DbSet<Inhabitant> Inhabitants { get; set; }
    }
}
