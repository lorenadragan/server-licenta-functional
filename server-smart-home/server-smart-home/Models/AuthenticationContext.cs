﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace server_smart_home.Models
{
    public class AuthenticationContext : IdentityDbContext
    {
        /// <summary>
        /// options = values like provider: SQLServer, Postgresql, and the ConnectionString
        /// </summary>
        /// <param name="options"></param>
        public AuthenticationContext(DbContextOptions<AuthenticationContext> options) : base(options)
        {

        }

        public DbSet<ApplicationUser> applicationUsers { get; set; }
    }
}
