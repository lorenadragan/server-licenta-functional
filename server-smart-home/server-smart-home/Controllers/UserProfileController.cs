﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using server_smart_home.Models;
using server_smart_home.Models.DTOs;
using server_smart_home.Repository;

namespace server_smart_home.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private UserManager<ApplicationUser> _userManager;
        private AuthenticationContext _authContext;
        private InhabitantsService _inhabitantsService;
        private HousesService _housesService;
        public UserProfileController(UserManager<ApplicationUser> userManager,
            AuthenticationContext authenticationContext,
            SmartHouseContext smartHouseContext)
        {
            this._userManager = userManager;
            this._authContext = authenticationContext;
            this._inhabitantsService = new InhabitantsService(smartHouseContext);
            this._housesService = new HousesService(userManager, smartHouseContext);
        }

        [HttpGet]
        [Authorize]
        public async Task<HomePageProfileInformation> GetUserProfile()
        {
            try
            {
                string userID = User.Claims.First(claim => claim.Type == "UserID").Value;
                var user = await this._userManager.FindByIdAsync(userID);
                var details = await this._housesService.GetHouseDTODetailsForHomePage(user.AccessCode);
                return new HomePageProfileInformation()
                {
                    AccessCode = user.AccessCode,
                    Email = user.Email,
                    UserName = user.UserName,
                    FullName = user.FullName,
                    HouseDetails = details
                };
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }

        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("GetAllClients")]
        public List<ClientProfileDTO> GetUsersProfiles()
        {
            //FullName, Email, UserName, AccessCode
            var users = this._userManager.Users.ToList();
            var clientsProfiles = new List<ClientProfileDTO>();
            foreach (var user in users)
            {
                if (!string.Equals(user.AccessCode, "test123456", StringComparison.InvariantCultureIgnoreCase))
                {
                    var clientProfile = new ClientProfileDTO()
                    {
                        FullName = user.FullName,
                        Email = user.Email,
                        AccessCode = user.AccessCode,
                        UserName = user.UserName,
                        Id = user.Id
                    };
                    clientsProfiles.Add(clientProfile);
                }
            }

            return clientsProfiles;
        }

        [HttpDelete]
        [Authorize(Roles = "Admin")]
        [Route("DeleteClient/{id}")]
        public OkObjectResult DeleteClient(string id)
        {
            try
            {
                //var userToDelete = await this._userManager.FindByIdAsync(id);
                var users = this._userManager.Users.ToList();
                //var users = this._authContext.applicationUsers.ToList();

                var userToDelete = users.Where(user => user.Id == id).First();
                this._authContext.applicationUsers.Remove(userToDelete);
                this._authContext.SaveChanges();

                //trebuie sa sterg si din tabelul de inhabitants
                this._inhabitantsService.DeleteInhabitantByUserId(id);

                return Ok("Deleted successfully");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPut]
        [Authorize(Roles = "Customer")]
        [Route("Update/User")]
        public async Task<IActionResult> UpdateUserProfile([FromBody] UpdateModel model)
        {
            string currentUserId = User.Claims.First(claim => claim.Type == "UserID").Value;
            var user = await this._userManager.FindByIdAsync(currentUserId);
            if (await this._userManager.CheckPasswordAsync(user, model.Password))
            {
                //parola a fost corecta, se poate face operatia de update;
                user.UserName = model.UserName;
                user.FullName = model.FullName;
                await this._userManager.UpdateAsync(user);
                return Ok(new { message = "Successfully updated." });
            }
            else
            {
                return BadRequest(new { message = "Password is incorrect." });
            }

        }

        [HttpPut]
        [Authorize(Roles = "Admin")]
        [Route("Admin/UpdateUser")]
        public async Task<IActionResult> UpdateUserFromAdminPanel([FromBody] AdminUpdateCustomerModel model)
        {
            try
            {
                var user = await this._userManager.FindByIdAsync(model.Id);
                if (user != null)
                {
                    user.UserName = model.UserName;
                    user.FullName = model.FullName;
                    user.AccessCode = model.AccessCode;
                    user.Email = model.Email;
                    await this._userManager.UpdateAsync(user);
                    return Ok(new { message = "Successfully updated." });
                }
                else
                {
                    return BadRequest(new { message = "Bad Request" });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("NewClient")]
        [Authorize(Roles = "Admin")]
        public async Task<object> CreateNewClientProfile(ApplicationUserModel userModel)
        {
            try
            {
                userModel.Role = "Customer";
                var givenAccessCode = userModel.AccessCode;
                var houseWithGivenCode = this._housesService.GetHouseWithGivenAccessCode(givenAccessCode);
                if (houseWithGivenCode != null)
                {
                    var applicationUser = new ApplicationUser()
                    {
                        UserName = userModel.UserName,
                        Email = userModel.Email,
                        FullName = userModel.FullName,
                        AccessCode = userModel.AccessCode
                    };

                    //adaug in tabela de user de la .NET
                    var result = await this._userManager.CreateAsync(applicationUser, userModel.Password);
                    await this._userManager.AddToRoleAsync(applicationUser, userModel.Role);

                    //adaug si in tabela de inhabitants ai casei userul nou
                    var newInhabitant = new Inhabitant()
                    {
                        House_Id = this._housesService.GetIdOfHouseWithGivenCode(givenAccessCode),
                        User_Id = applicationUser.Id
                    };
                    this._inhabitantsService.AddNewInhabitant(newInhabitant);

                    return Ok(result);
                }
                else
                {
                    return BadRequest("There is no house with given access code.");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("Add/AccessCode")]
        public IActionResult AddNewHouse([FromBody] HouseModel model)
        {
            try
            {
                this._housesService.AddHouse(model);

                return Ok(new { message = "New House Created Successfully." });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("GetHouses/AdminPanel")]
        public IActionResult GetHousesForAdminPanel()
        {
            try
            {
                var houses = this._housesService.GetHousesForAdminPanel();

                return Ok(houses);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}