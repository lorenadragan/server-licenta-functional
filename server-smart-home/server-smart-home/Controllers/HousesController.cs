﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using server_smart_home.Models.DTOs;

namespace server_smart_home.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HousesController : ControllerBase
    {
        private readonly string baseURI = "http://192.168.4.1/";
        public HousesController()
        {
        }

        //test turn on led de pe placa
        //GET: api/led_placa/on
        //GET api/led_placa/off
        [HttpGet]
        [Route("led_placa/{onoff}")]
        public async void LedPlaca(string onoff)
        {
            try
            {
                int placa = -1;
                if (String.Equals(onoff, "on", StringComparison.InvariantCultureIgnoreCase))
                {
                    placa = 0;
                }
                else
                {
                    placa = 1;
                }
                HttpClient client = new HttpClient();
                await client.GetAsync(baseURI + "/sensors/led_placa=" + onoff + "/end");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET: api/Houses
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("GetAllHouses")]
        public object Get()
        {
            return null;
        }

        // GET: api/Houses/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Houses
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("AddHouse")]
        public object Post([FromBody] HouseDTO newHouse)
        {

            return null;
        }

        // PUT: api/Houses/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}