﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using server_smart_home.Models;
using server_smart_home.Repository;

namespace server_smart_home.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationUserController : ControllerBase
    {
        private UserManager<ApplicationUser> _userManager;
        private IOptions<ApplicationSettings> _appSettings;
        private HousesService _housesService;
        private InhabitantsService _inhabitantsService;

        public ApplicationUserController(SmartHouseContext smartHouseContext, UserManager<ApplicationUser> userManager, IOptions<ApplicationSettings> appSettings)
        {
            this._appSettings = appSettings;
            this._userManager = userManager;
            this._housesService = new HousesService(userManager, smartHouseContext);
            this._inhabitantsService = new InhabitantsService(smartHouseContext);
        }


        /// <summary>
        /// Registration method
        /// POST: api/UserApplication/Register
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Register")]
        public async Task<object> RegisterMethod(ApplicationUserModel userModel)
        {
            try
            {
                if (!string.Equals(userModel.Role, "Admin", StringComparison.InvariantCultureIgnoreCase))
                {
                    var givenAccessCode = userModel.AccessCode;
                    var houseWithGivenCode = this._housesService.GetHouseWithGivenAccessCode(givenAccessCode);
                    if (houseWithGivenCode != null)
                    {
                        var applicationUser = new ApplicationUser()
                        {
                            UserName = userModel.UserName,
                            Email = userModel.Email,
                            FullName = userModel.FullName,
                            AccessCode = userModel.AccessCode,
                        };

                        //adaug user in tabela de user de la .NET
                        var result = await _userManager.CreateAsync(applicationUser, userModel.Password);
                        await _userManager.AddToRoleAsync(applicationUser, userModel.Role);

                        //trebuie sa adaug si in tabela de inhabitants ai casei userul nou
                        var newInhabitant = new Inhabitant()
                        {
                            House_Id = this._housesService.GetIdOfHouseWithGivenCode(givenAccessCode),
                            User_Id = applicationUser.Id
                        };
                        this._inhabitantsService.AddNewInhabitant(newInhabitant);

                        return Ok(result);
                    }
                    else
                    {
                        return BadRequest("There is no house with given access code");
                    }
                }
                else
                {
                    //trebuie sa inregistrez un admin, doar pentru test
                    var applicationUser = new ApplicationUser()
                    {
                        UserName = userModel.UserName,
                        Email = userModel.Email,
                        FullName = userModel.FullName,
                        AccessCode = userModel.AccessCode,
                    };

                    var result = await _userManager.CreateAsync(applicationUser, userModel.Password);
                    await _userManager.AddToRoleAsync(applicationUser, userModel.Role);
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Authentication Method
        /// Post: ApplicationUser/Login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> LoginMethod([FromBody] LoginModel model)
        {
            //folosim user manager pentru a verifica daca exista un utilizator cu credentialele trimise (username)
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password)) //if the user with given username exists and the password is correct
            {
                //Get the role assigned to the user
                var role = await _userManager.GetRolesAsync(user);
                IdentityOptions _options = new IdentityOptions();
                //generate the token descriptor 
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID", user.Id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType, role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddDays(300),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this._appSettings.Value.JWT_Secret)), SecurityAlgorithms.HmacSha256)
                };

                //generate token handler
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);

                //now create the token
                var token = tokenHandler.WriteToken(securityToken);

                return Ok(new { token = token });
            }
            else
            {
                return BadRequest(new { message = "Username or password provided is not correct." });
            }
        }
    }
}