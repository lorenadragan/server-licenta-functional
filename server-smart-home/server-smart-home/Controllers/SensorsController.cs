﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using server_smart_home.Models;

namespace server_smart_home.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorsController : ControllerBase
    {
        private HttpClient _httpClient;
        private SmartHouseContext _context;
        //private readonly string baseURI = "http://192.168.4.1/";
        public SensorsController(SmartHouseContext smartHouseContext)
        {
            this._context = smartHouseContext;
            this._httpClient = new HttpClient();
            this._httpClient.Timeout = TimeSpan.FromMinutes(30);
            this._httpClient.DefaultRequestHeaders.Add("Connection", "keep-alive");
            this._httpClient.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            this._httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, br");
            this._httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
            this._httpClient.DefaultRequestHeaders.Add("Accept-Language", "ro-RO,ro;q=0.9,en-US;q=0.8,en;q=0.7");
            this._httpClient.DefaultRequestHeaders.Add("Host", "192.168.4.1");
        }


        [HttpGet]
        [Route("photoresistor-led/{command}")]
        public void ControlDualLedSimpleCommands(string command)
        {
            //handles on/off comands or is_auto command
            if (String.Equals(command, "off", StringComparison.InvariantCultureIgnoreCase))
            {

                var jsonAsString = "";
                try
                {
                    using (HttpResponseMessage response = this._httpClient.GetAsync(this._context.baseURIDefinition + "sen/ph/is_off/end").Result)
                    {

                        using (HttpContent content = response.Content)
                        {
                            jsonAsString = content.ReadAsStringAsync().Result;
                            Debug.WriteLine("Entered");
                            //_sensorsHub.Clients.All.SendAsync("transfersensorsdata", jsonAsString);
                            jsonAsString = "";
                            //this._logger.LogInformation(jsonAsString);

                        }
                    }
                }
                catch (TaskCanceledException ex)
                {
                    Debug.WriteLine(ex.CancellationToken.IsCancellationRequested);
                }
            }

            if (String.Equals(command, "on", StringComparison.InvariantCultureIgnoreCase))
            {

                var jsonAsString = "";
                try
                {
                    using (HttpResponseMessage response = this._httpClient.GetAsync(this._context.baseURIDefinition + "sen/ph/is_on/end").Result)
                    {

                        using (HttpContent content = response.Content)
                        {
                            jsonAsString = content.ReadAsStringAsync().Result;
                            Debug.WriteLine("Entered");
                            //_sensorsHub.Clients.All.SendAsync("transfersensorsdata", jsonAsString);
                            jsonAsString = "";
                            //this._logger.LogInformation(jsonAsString);

                        }
                    }
                }
                catch (TaskCanceledException ex)
                {
                    Debug.WriteLine(ex.CancellationToken.IsCancellationRequested);
                }
            }

            if (String.Equals(command, "auto", StringComparison.InvariantCultureIgnoreCase))
            {
                var jsonAsString = "";
                try
                {
                    using (HttpResponseMessage response = this._httpClient.GetAsync(this._context.baseURIDefinition + "sen/ph/is_auto/end").Result)
                    {

                        using (HttpContent content = response.Content)
                        {
                            jsonAsString = content.ReadAsStringAsync().Result;
                            Debug.WriteLine("Entered");
                            //_sensorsHub.Clients.All.SendAsync("transfersensorsdata", jsonAsString);
                            jsonAsString = "";
                            //this._logger.LogInformation(jsonAsString);

                        }
                    }
                }
                catch (TaskCanceledException ex)
                {
                    Debug.WriteLine(ex.CancellationToken.IsCancellationRequested);
                }
            }
        }

        //api/houses/photoresistor-led/manual/red=123/green=123
        [HttpGet]
        [Route("photoresistor-led/manual/red={redvalue}/green={greenvalue}")]
        public void ControlDualLedManualSetup(string redvalue, string greenvalue)
        {
            //var res = _httpClient.GetAsync(baseURI + "sen/ph/man/r=" + redvalue + "/a/g=" + greenvalue + "/end").Result;
            var jsonAsString = "";
            try
            {
                using (HttpResponseMessage response = this._httpClient.GetAsync(this._context.baseURIDefinition + "sen/ph/man/r=" + redvalue + "/a/g=" + greenvalue + "/end").Result)
                {

                    using (HttpContent content = response.Content)
                    {
                        jsonAsString = content.ReadAsStringAsync().Result;
                        Debug.WriteLine("Entered");
                        //_sensorsHub.Clients.All.SendAsync("transfersensorsdata", jsonAsString);
                        jsonAsString = "";
                        //this._logger.LogInformation(jsonAsString);

                    }
                }
            }
            catch (TaskCanceledException ex)
            {
                Debug.WriteLine(ex.CancellationToken.IsCancellationRequested);
            }
        }

        [HttpGet]
        [Route("smd-rgb/{command}")]
        public void ControlSmdSimpleCommand(string command)
        {
            if (String.Equals("auto", command, StringComparison.InvariantCultureIgnoreCase))
            {
                string jsonAsString = "";
                try
                {
                    using (HttpResponseMessage response = this._httpClient.GetAsync(this._context.baseURIDefinition + "sen/rgb/is_auto/end").Result)
                    {

                        using (HttpContent content = response.Content)
                        {
                            jsonAsString = content.ReadAsStringAsync().Result;
                            Debug.WriteLine("Entered");
                            //_sensorsHub.Clients.All.SendAsync("transfersensorsdata", jsonAsString);
                            jsonAsString = "";
                            //this._logger.LogInformation(jsonAsString);

                        }
                    }
                }
                catch (TaskCanceledException ex)
                {
                    Debug.WriteLine(ex.CancellationToken.IsCancellationRequested);
                }
            }
        }

        [HttpGet]
        [Route("rgb/manual/red={red}/green={green}/blue={blue}")]
        public void ControlSmdManualCommand(string red, string green, string blue)
        {
            string jsonAsString = "";
            try
            {
                using (HttpResponseMessage response = this._httpClient.GetAsync(this._context.baseURIDefinition + "sen/rgb/r=" + red + "/a/g=" + green + "/a/b=" + blue + "/end").Result)
                {

                    using (HttpContent content = response.Content)
                    {
                        jsonAsString = content.ReadAsStringAsync().Result;
                        Debug.WriteLine("Entered");
                        //_sensorsHub.Clients.All.SendAsync("transfersensorsdata", jsonAsString);
                        jsonAsString = "";
                        //this._logger.LogInformation(jsonAsString);

                    }
                }
            }
            catch (TaskCanceledException ex)
            {
                Debug.WriteLine(ex.CancellationToken.IsCancellationRequested);
            }
        }
    }
}